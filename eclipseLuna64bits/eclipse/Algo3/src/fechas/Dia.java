package fechas;

public class Dia 
{
       private String nombre;
       private int valor;
       
       public Dia()
     {
    	  nombre=new String();
    	  valor=0;
     }
     public Dia(String nombre, int valor)
     { 
    	 this.nombre=nombre;
         this.valor=valor;
     }
      public Dia(String nombre)
         { 
        	 this.nombre=nombre;
             this.valor=diaValor(nombre);
     }
      private int diaValor(String nombre)
      {
    	  int valor = 0;
    	  String nombreMinuscula=nombre.toLowerCase();
    	  
          if(nombreMinuscula.equals("lunes")) valor=1;
          if(nombreMinuscula.equals("martes")) valor=2;
          if(nombreMinuscula.equals("miercoles")) valor=3;
          if(nombreMinuscula.equals("jueves")) valor=4;
          if(nombreMinuscula.equals("viernes")) valor=5;
          if(nombreMinuscula.equals("sabado")) valor=6;
          if(nombreMinuscula.equals("domingo")) valor=7;
		return valor;
         
          
      }
     public void verDatos()
     {
    	System.out.println("dia: "+nombre);
    	System.out.println("numero de dia: "+ valor);
   }
}
