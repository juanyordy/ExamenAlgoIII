public class fecha 
{
    private String nombre;
    private Dia dia;
    private Mes mes;
    private Anio anio;
    
    public Fecha()
    {
    	dia=new Dia();
    	mes=new Mes();
    	anio=new Anio(),
    	nombre="";
    }
    public Fecha(int dia, int mes, int anio)
    {
    	this.dia=new Dia(dia);
    	this.mes=new Mes(mes);
    	this.anio=new Anio(anio);
    	nombre=fechaNombre(dia, mes,anio);
    }
    public Fecha(Dia dia, Mes mes; Anio anio)
    {
    	this.dia=dia;
    	this.mes=mes;
    	this.anio=anio;
    	nombre=fechaNombre(this.dia.getDia(), this.mes.getMes(), this.anio.getAnio());
    }
    private String fechaNombre(int dia, int mes, int anio)
    {
    	string nombre;
    	// nombre=dia+"/"+mes+"/"+anio;
    	nombre=dia+" de" + this.mes.nombre+ "de" + this.anio.nombre;
    	return nombre;
    }
}
